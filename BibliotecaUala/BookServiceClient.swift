//
//  BookServiceClient.swift
//  BibliotecaUala
//
//  Created by Daniel on 17/08/2019.
//  Copyright © 2019 Daniel. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

// MARK: -
protocol BookServiceClientDelegate: class {
    func getAllBooksDidFinish(books: JSON)
}

// MARK: -
class BookServiceClient {
    weak var delegate: BookServiceClientDelegate?
    var data: JSON!
    
    func getAllBooks(url: String) {
        Alamofire.request("https://qodyhvpf8b.execute-api.us-east-1.amazonaws.com/test/books").responseJSON { response in
            switch response.result {
            case .success(let data):
                self.data = JSON(data)
                self.delegate?.getAllBooksDidFinish(books: self.data)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
