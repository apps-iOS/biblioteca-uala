//
//  ViewController.swift
//  BibliotecaUala
//
//  Created by Daniel on 17/08/2019.
//  Copyright © 2019 Daniel. All rights reserved.
//

import UIKit
import AlamofireImage
import SwiftyJSON

class ViewController: UIViewController {

    let bookServiceClient = BookServiceClient()
    var booksJSON: JSON!
    var booksArray: Array<JSON> = []
    var bookDetail: JSON!
    @IBOutlet weak var booksTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.booksTableView.dataSource = self
        self.booksTableView.delegate = self
        self.bookServiceClient.delegate = self
        
        self.bookServiceClient.getAllBooks(url: "https://qodyhvpf8b.execute-api.us-east-1.amazonaws.com/test/books")
    }
    
    func sortDesc() {
        self.booksArray.sort { $0["popularidad"] > $1["popularidad"] }
        print(self.booksArray)
        self.booksTableView.reloadData()
    }
    
    func sortAsc() {
        self.booksArray.sort { $0["popularidad"] < $1["popularidad"] }
        print(self.booksArray)
        self.booksTableView.reloadData()
    }
}

extension ViewController: BookServiceClientDelegate {
    func getAllBooksDidFinish(books: JSON) {
        for book in books.array! {
            self.booksArray.append(book)
        }
        self.sortDesc()
        self.booksTableView.reloadData()
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.booksArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! BookTableViewCell
        let book: JSON = self.booksArray[indexPath.row]
        
        if let imageURL = URL(string: book["imagen"].rawString()!), let placeholder = UIImage(named: "book.jpg") {
            cell.imageBook.af_setImage(withURL: imageURL, placeholderImage: placeholder)
        }
        cell.labelTitle.text = book["nombre"].rawString()
        cell.labelAutor.text = "Autor: \(book["autor"])"
        cell.labelPopularidad.text = "Popularidad: \(book["popularidad"])"
        cell.labelDisponibilidad.text = book["disponibilidad"].boolValue ? "Disponible" : "No disponible"
        cell.labelDisponibilidad.textColor = book["disponibilidad"].boolValue ? UIColor.green : UIColor.red
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    

    private func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.bookDetail = self.booksArray[indexPath.row]
        performSegue(withIdentifier: "detail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let bookDetailViewController = segue.destination as! BookDetailViewController
        bookDetailViewController.bookDetail = self.bookDetail
    }

}

