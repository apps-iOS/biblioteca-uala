//
//  BookDetailViewController.swift
//  BibliotecaUala
//
//  Created by Daniel on 17/08/2019.
//  Copyright © 2019 Daniel. All rights reserved.
//

import UIKit
import SwiftyJSON
import AlamofireImage

class BookDetailViewController: UIViewController {
    var bookDetail: JSON!
    @IBOutlet weak var imageBook: UIImageView!
    @IBOutlet weak var labelTitulo: UILabel!
    @IBOutlet weak var labelAutor: UILabel!
    @IBOutlet weak var labelPopularidad: UILabel!
    @IBOutlet weak var labelDisponibilidad: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(self.bookDetail)
        // self.loadDetail()

    }
    
    func loadDetail() {
        if let imageURL = URL(string: self.bookDetail["imagen"].rawString()!), let placeholder = UIImage(named: "book.jpg") {
            self.imageBook.af_setImage(withURL: imageURL, placeholderImage: placeholder)
        }
        
        self.labelTitulo.text = self.bookDetail["nombre"].rawString()
        self.labelAutor.text = "Autor: \(self.bookDetail["autor"])"
        self.labelPopularidad.text = "Popularidad: \(self.bookDetail["popularidad"])"
        self.labelDisponibilidad.text = self.bookDetail["disponibilidad"].boolValue ? "Disponible" : "No disponible"
        self.labelDisponibilidad.textColor = self.bookDetail["disponibilidad"].boolValue ? UIColor.green : UIColor.red
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
