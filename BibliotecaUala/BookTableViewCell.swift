//
//  BookTableViewCell.swift
//  BibliotecaUala
//
//  Created by Daniel on 17/08/2019.
//  Copyright © 2019 Daniel. All rights reserved.
//

import UIKit

class BookTableViewCell: UITableViewCell {

    @IBOutlet weak var imageBook: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelAutor: UILabel!
    @IBOutlet weak var labelPopularidad: UILabel!
    @IBOutlet weak var labelDisponibilidad: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
